import scalaz._
import Scalaz._
import effect._
import IO._
import scala.annotation.tailrec

object IOAction {
  val put: String => IOAction[Unit] =
    s => Put(s, Return(()))

  val get: IOAction[String] =
    Get(s => Return(s))

  def seqIO[A, B](io: IOAction[A], f: A => IOAction[B]): IOAction[B] =
    io match {
      case Return(a)  => f(a)
      case Put(s, io) => Put(s, seqIO(io, f))
      case Get(g)     => Get(s => seqIO(g(s), f))
    }

  val echo1 =
    seqIO(get, put)

  val ask1: String => IOAction[String] =
    s => seqIO(put(s), (_: Unit) => get)

  @tailrec
  def run[A](io: IOAction[A]): A =
    io match {
      case Return(a) => a
      case Put(s, io) =>
        Console.println(s)
        run(io)
      case Get(f) => run(f(Console.readLine()))
    }

  def runIO[A](io: IOAction[A]): IO[A] =
    io match {
      case Return(a)  => a.point[IO]
      case Put(s, io) => putStrLn(s) >> runIO(io)
      case Get(g)     => readLn >>= g andThen runIO
    }

  case class State(in: Vector[String], out: Vector[String] = Vector())

  @tailrec
  def test[A](io: IOAction[A], in: Seq[String], out: Seq[String] = Vector()): (Seq[String], Seq[String], A) =
    io match {
      case Return(a)  => (in, out, a)
      case Put(s, io) => test(io, in, out :+ s)
      case Get(f)     => test(f(in.head), in.tail, out)
    }

  implicit val IOActionMonad = new Monad[IOAction] {
    def point[A](a: => A): IOAction[A] =
      Return(a)

    def bind[A, B](fa: IOAction[A])(f: A => IOAction[B]): IOAction[B] =
      seqIO(fa, f)
  }

  val ask2 = (s: String) =>
    for {
      _ <- put(s)
      a <- get
    } yield a

  val echo2 = get >>= put

  val ask: String => IOAction[String] =
    put(_) >> get

  val getNameAge: IOAction[(String, Int)] =
    for {
      name <- ask("what's your name?")
      year <- ask("what year were you born?") map { _.toInt }
      age = 2013 - year
    } yield (name, age)

  val writeHowOld: IOAction[Unit] =
    for {
      (n, a) <- getNameAge
      _ <- put(s"Hi, $n. You'll turn $a this year!")
    } yield ()
}

sealed trait IOAction[A]
case class Return[A](a: A) extends IOAction[A]
case class Put[A](s: String, io: IOAction[A]) extends IOAction[A]
case class Get[A](f: String => IOAction[A]) extends IOAction[A]
