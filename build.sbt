name := "io-demo"

description := "demonstration of pure IO, from the haskell in http://chris-taylor.github.com/blog/2013/02/09/io-is-not-a-side-effect/"

scalaVersion in ThisBuild := "2.10.0"

scalacOptions in ThisBuild ++= Seq("-deprecation", "-unchecked", "-feature", "-language:_")

libraryDependencies ++= Seq(
  "org.scalaz" %% "scalaz-core"   % "7.0.0-M7"
, "org.scalaz" %% "scalaz-effect" % "7.0.0-M7"
)

initialCommands in console := """import IOAction._"""
