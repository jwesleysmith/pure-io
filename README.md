Pure, Functional IO.
====================
Presentation on how IO is done in a pure functional manner.

Contains a keynote presentation and the accompanying Scala code.
The code can be built and run with SBT.

Also contains [a PDF version of the presentation](https://bitbucket.org/jwesleysmith/pure-io/src/master/src/main/pdf/pure-io.pdf).